# ed(1)

## Introduction

Hi, my name is David Jones; my pronouns are he him.
I'm excited to have this opportunity to talk to you today.

About `ed(1)`.

## What is ed(1)?

`ed(1)` was created in 1969[1], and was first documented in
The UNIX Programmer's Manual 1st edition[2]
published in 1971.

Here we have an extract from that manual:

Slide:

    ed is the standard text editor. ed is based on QED

So, two fun facts here:
1) ed is the standard text editor; and,
2) ed is based on QED.

`ed(1)` has been in every Unix ever since, and in every Unix standard.

If I'm giving this presentation in 2019,
it's the 50th anniversary year of `ed(1)`!

`ed(1)` is a _line editor_.
Meaning:

1) A file is a sequence of lines;
2) editing commands apply to a line;
3) Input and Output are line-based, not _full screen_.

Maybe you're familiar with a _full screen_ editor like `vi` or `nano`.
Maybe you've even used a GUI editor like `gvim` or `atom` or `mu`.

The editing model of `ed(1)` is a simplification of qed
and has gone on to inform the model of sed, ex, vi, sam and probably more.
grep(1) is based on the `g` command in `ed(1)`, and
borrowed its regular expression syntax.
The same regular expression syntax was borrowed by
awk, perl, JavaScript, Python,
and practically every modern language in use today.

## Why learn ed

Slide:

    It's fast.
    It's scriptable.
    It avoids clearing your screen.
    It works without a lot of shared libraries.
    It probably works when the rest of your system doesn't.

## Let's learn ed

Sacrifice your mouse, close your windows, embrace the text console.

Back in the day, `ed(1)` was designed to be used on teletype terminals.
Think of this as a typewriter with enough mechanical gadgets attached
that it could also be made to type under the control of a telephone modem.

Slide:
    [picture of ADM-33]

These were slow, and pretty noisy.
Consequently, `ed(1)` is very terse.

## Following Kernighan's tutorial

Append to an empty buffer.

error messages

Kernighan described the error message as
"about as cryptic as it can be, but with practice,
you can usually figure out how you goofed"

w

writes files

q e

quit and edit

[we don't need r and f in a short intro]

1 $ p

Numeric addresses, dollar, printing lines.

. + -

dot and relative addresses.

=

Finding out the number of lines, and the current line number.

d

deleting lines.

Now we can delete and append lines,
we have a complete set,
allowing any edits to any file.

Let's recap the addresses and commands of `ed(1)`.

The commands in `ed(1)` take the form:

Slide:

    address1,address2 command arguments

    p  short for  .p  short for .,. p

    ,p  short for 1,$p

Most elements are optional,
and you can put spaces in to separate most elements if you want.

So we see that an address can be a line number or dollar.
There is another sort of address,
that allows us to find a line by context.
We can find a line by searching the text inside it.

If we want to find a line that contains «better» we use:

    /better/

These context addresses are interchangeable with line numbers.
Just as typing 2 on its own will result in moving to line 2 and
printing it out, so will using a context address.

Searches proceed forwards from the _next_ line,
and if the search reaches the end of the buffer,
it wraps around to carry on from the first line of the buffer.

If you use question marks:

    ?better?

Then the search proceeds backwards, starting from the
previous line, and still wraps around.

If `ed(1)` doesn't find a line at all,
it responds with the, by now familiar, terse diagnostic.

    /Zen
    ?

This example also shows use that search in `ed(1)` is
case-sensitive.
A search for `/Zen/` with a capital `Z`,
does not find `zen` with a little `z`.
There are no options to change that, in standard `ed(1)`.

Search expressions can also appear as arguments to commands.
`s` stands for substitute,
and is the command that changes text within a line,
as opposed to appending or deleting lines.

    s/thier/their/

will change the leftmost occurence of "thier" with "their".

Of course this is a popular command and there are lots of
variations.
Adding a `g` suffix makes the substitution repeat for all
occurrences on the addressed line.
Adding a `p` suffix prints the line after the substitution.

An empty search expression means
"the most recently used search expression".
So if you want to first find the line containing "zeb"
and change it, you could use:

    /zeb/s//zen/p

if you miss the final slash of a substitute command, it's the
same as using the `p` suffix.
The line is printed.

In a substitute command you can use any character
(except whitespace) to delimit the search expression,
so to replace all slashes with colons:

    s[/[:[g

Note you can't do that with context addresses, `ed(1)` wouldn't know
whether to search forwards or backwards.

Of course it turns out that the search expressions are
regular expressions, using hat dot dollar * square bracket and
backslash as special characters.

I don't have time to cover that here, but the same regular expressions
pop up all over Unix and Python and you may already be familiar
with them.

I hope you can now enjoy using the standard editor.


## REFERENCES

[1] Not in "Evolution of the UNIX Time Sharing System", 1980 (reprinted 1984).
There are hints in this paper that there is a PDP 7 version of Unix in 1969,
and that it had an editor, but they don't explicitly say it is ed.
The available ed source code dates from 1970.

[2] UNIX Programmer's Manual, 1st Edition.

[3] "A Tutorial Introduction to the UNIX Text Editor",
Brian Kernighan.

## Further reading


